# Fiche thématique

## Architectures matérielles et logicielles

Thème commun aux deux niveux : Première et Terminale.

### [Programme officiel (Première)](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Modèle d’architecture séquentielle (von Neumann) | Distinguer les rôles et les caractéristiques des différents constituants d’une machine. <br>Dérouler l’exécution d’une séquence d’instructions simples du type langage machine. | La présentation se limite aux concepts généraux. <br>On distingue les architectures monoprocesseur et les architectures multiprocesseur. <br>Des activités débranchées sont proposées. Les circuits combinatoires réalisent des fonctions booléennes. |
| Systèmes d’exploitation | Identifier les fonctions d’un système d’exploitation.<br>Utiliser les commandes de base en ligne de commande.<br>Gérer les droits et permissions d’accès aux fichiers. | Les différences entre systèmes <br>d’exploitation libres et propriétaires sont évoquées. Les élèves utilisent un système d’exploitation libre. <br>Il ne s’agit pas d’une étude théorique des systèmes d’exploitation.




### [programme officiel (Terminale)](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf) 

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Composants intégrés d’un système sur puce.  | Identifier les principaux composants sur un schéma de circuit et les avantages de leur intégration en termes de vitesse et de consommation. | Le circuit d’un téléphone peut être pris comme un exemple : <br>microprocesseurs, mémoires locales, interfaces radio et filaires, gestion d’énergie, contrôleurs vidéo, accélérateur graphique, réseaux sur puce, etc.
| Gestion des processus et des ressources par un système d’exploitation. |Décrire la création d’un processus, l’ordonnancement de plusieurs processus par le système. <br>Mettre en évidence le risque de l’interblocage (deadlock). | À l’aide d’outils standard, il s’agit d’observer les processus actifs ou en attente sur une machine. <br>Une présentation débranchée de l’interblocage peut être proposée.


