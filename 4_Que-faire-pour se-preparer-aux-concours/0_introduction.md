Ce bloc a très peu de contenu car c'est sur le site partenaire <a href="https://capes-nsi.org" target="_blank">https://capes-nsi.org</a> que se trouvent toutes les ressources pour se préparer au CAPES. 

## Un site dédié à la préparation au CAPES

[![Site du jury du CAPES externe NSI](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/4_Que-faire-pour%20se-preparer-aux-concours/Ressources/baniere-capes-nsi-org.png)](https://capes-nsi.org/)

En toute transparence et pour maximiser l'égalité des chances, on y trouve la description des épreuves, la liste des outils logiciels utilisés et des ressources avec des listes de sujets des épreuves pour s'entraîner, les rapports du jury et recommandations pour se positionner.

## Qu'offrons nous en plus ici ?

### Une double formation

Une double formation disponible à la fois sur le <a href="https://mooc-nsi-snt.gitlab.io/portail/0_Accueil.html" target="_blank">portail des formations NSI</a> et sur la plateforme <a href="https://www.fun-mooc.fr/fr/" target="_blank">FUN-Mooc</a>. Cette double formation permet d'aborder :

- les fondements de l'informatique 

    - <a href="https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/" target="_blank">via FUN</a> 
[![Numérique et Sciences Informatiques : les fondamentaux](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/4_Que-faire-pour%20se-preparer-aux-concours/Ressources/NSI-vignette-Mooc1.png)](https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/)
    - sur le portail : <a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html" target="_blank">https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html</a> 
    
- et l'apprentissage de l'enseignement

    - <a href="https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/" target="_blank">via FUN</a> 
[![Apprendre à enseigner le Numérique et les Sciences Informatiques](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/4_Que-faire-pour%20se-preparer-aux-concours/Ressources/NSI-vignette-Mooc2.png)](https://www.fun-mooc.fr/fr/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/)
    - sur le portail : <a href="https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/index.html" target="_blank">https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/index.html</a>

### Un service

Ici nous offrons surtout un service. L'idée est de 

- **s'entraider à travers le** [**forum NSI-SNT**](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html), notamment via les catégories [préparer le capes NSI](https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/preparer-le-capes-nsi/135), [se former NSI + SNT](https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/se-former-a-la-nsi-snt/136), [agrégation informatique](https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/agregation-informatique/190). _Nous pourrons créer de nouvelles catégories en fonction de vos besoins._

- **vous aider, vous renseigner, vous accompagner** : sur le [forum](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html) avec des messages individuels si besoin ou en [nous contactant](https://mooc-nsi-snt.gitlab.io/portail/7_Contact.html) directement.
Bien entendu nous nous devons de traiter chacune et chacun de manière égalitaire, nous n'allons donc pas accompagner certaines personnes et pas d'autres, chaque fois que nous pourrons conseiller, partager, aider, nous remettrons immédiatement le contenu en partage, sur le forum ou sur ces pages... tout particulièrement dans la [foire aux questions](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/Le_Mooc/4_Que-faire-pour%20se-preparer-aux-concours/8_Foire_aux_questions.html).

## Pas de contenus spécifique ?

Pas besoin :) Toutes les ressources sont là, et la plus value ici est de vous accompagner. 

C'est donc à vous de poser les questions qui vous interessent et d'exprimer vos besoins !

Nous comptons donc sur vous et vos retours pour faire que ce bloc réponde aux mieux à vos attentes et devienne utile à toute un chacune et tout un chacun voulant se préparer aux concours.

Il va y avoir de plus en plus de profs d'informatique, et donc plus que sélectionner telle ou tel à un concours ou un autre, l'enjeu est ici de se préparer au mieux. **En s'entraidant on maximisera les chances de réussir les épreuves demandées.**
