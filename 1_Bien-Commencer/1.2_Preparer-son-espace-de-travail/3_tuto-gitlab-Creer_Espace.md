# Créer son espace à partir d'un modèle

La démarche est simple : nous avons déjà créé un espace projet : **MOOC 2 _ Ressources** dans [“MOOC.NSI_SNT”](https://gitlab.com/mooc-nsi-snt) que nous vous proposons tout simplement de “dupliquer". Une fois cette opération effectuée, vous aurez votre propre espace de travail dans lequel vous pourrez travailler à loisir.
**Comment faire ?**


## 1. Se connecter / S'inscrire
Pour commencer, s'[incrire à GitLab](https://gitlab.com/users/sign_up) ou se [connecter](https://gitlab.com/users/sign_in) si vous avez déjà un compte


## 2. Dupliquer l'espace projet NSI ressources

Accéder à la [page du projet](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources).

Maintenant vous allez créer une version de cet espace _chez vous_ ie dans votre propre espace _gitlab_ ; dans le _jargon_ git cette opération se nomme **_fork_** (ou _Créer une divergence_ en français) c'est d'ailleurs le nom du bouton sur lequel il va falloir cliquer (ci-dessous pour la version en anglais) : ![fork](fork.png)

Au moment de la création de ce _fork_ :
- ne pas modifier le nom du projet
- sélectionner votre nom d'espace
- penser à personnaliser votre _description de projet_
- il vous est proposé un choix de visibilité pour votre projet : il faut **choisir public pour pouvoir donner accès à votre travail**.

Un projet est maintenant créé xxx/mooc-2-ressources où xxx est votre pseudo Gitlab. C’est votre espace de travail pour tous les exercices à faire. Ce projet est public, vous pourrez donner des liens vers des fichiers de ce projet dans le forum pour échanger avec les autres élèves.

Dès lors vous pouvez accéder à votre espace dépôt qui aura pour url : `https://gitlab.com/votre_pseudo/mooc-2-ressources`. Et choisir la vue _web_ de votre espace en cliquant le bouton `Web IDE` :
![Bouton WEB IDE](webide_bouton.png)
    
Vous devriez maintenant avoir une interface similaire à celle-là :
![Interface WEB IDE](webide_interface.png)

Vous pouvez dès maintenant consulter le contenu de ce dépôt en cliquant sur le lien xxx/mooc-2-ressources. Un dossier par séquence a été créé, des fichiers sont présents pour vous aider, notamment, dans votre progression du **Bloc 2 : Mise en pratique professionnelle**. Vous les verrez en temps utile. 

---
__Note__: Pas besoin de demander un accès au dépôt https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/ qui est en lecture seule car c'est juste une racine à partir de laquelle chaque participant·e dérive sa propre copie comme détaillé ici.
---
