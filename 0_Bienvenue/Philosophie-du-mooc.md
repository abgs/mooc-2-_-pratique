# Organisation opérationnelle

Dans un MOOC, en général, se rencontrent deux publics :

- l'équipe pédagogique qui propose le contenu du MOOC (le cours et les exercices),
- les apprenants qui suivent le MOOC.

Ce MOOC est un peu différent :

- l'équipe pédagogique est plus une équipe d'architectes qui propose un squelette d'organisation mais n'intervient pas pour dispenser des cours dans le MOOC ;
- les personnes apprenantes (vous … et nous) sommes à la fois celles et ceux qui suivent le MOOC, et qui en font évoluer le contenu (par les ressources produites, artefacts des activités pour apprendre à produire une ressource d'enseignement)
- il y a un troisième public (invisible dans le MOOC) : les élèves des classes (réelles ou imaginées) à qui vous destinez les activités que vous préparez.

Dans la suite, le terme _élèves_ désignent bien des élèves de lycée à qui les _fiches élèves_ sont destinées. Le terme _prof_ désigne **vous** (et nous), enseignantes ou enseignants, en situation d'apprenants dans ce MOOC. Nous mêmes enseignons en informatique au niveau de l'enseignement secondaire ou universitaire.

Le MOOC est fondé sur la production de ressources type _fiches_ qui vont venir expliciter le pourquoi et le comment d'activités _élèves_.

Ces fiches sont réalisées au format texte _markdown_ (_md_) et déposées dans un gitlab dédié (nous reviendrons sur son utilisation) : elles pourront faire l'objet d'évaluations par les pairs (via la plateforme FUN) après avoir été discutées sur le [forum NSI-SNT](https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/se-former-a-la-nsi-snt/136).
